FROM registry.access.redhat.com/rhscl/php-72-rhel7

EXPOSE 8080

COPY . /opt/app-root/src
USER root
RUN chgrp -R 0 /opt/app-root/src && \
    chmod -R g=u /opt/app-root/src
USER 1001

CMD /bin/bash -c 'php -S 0.0.0.0:8080'